package com.xinda.xiaoxing.service;

import com.xinda.xiaoxing.config.opc.Connector;
import com.xinda.xiaoxing.entity.influxdb.OPC;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.Server;

import java.util.List;
import java.util.Map;

public interface OPCService {
    /**
     * 初始化要读取OPC服务器的点位
     * @param serverName
     * @param prefix
     * @return
     */
    Map<String, Item> initItems(String serverName, String prefix);

    /**
     * 读取设备数据
     * @param server 服务器
     * @param serverName 服务器名
     * @param idItemMap id对应设备的map
     */
    List<OPC> getOpcData(Server server, String serverName, Map<String, Item> idItemMap);

    /**
     * 将OPC服务器的数据同步到InfluxDB
     * @param server
     * @param connectionListener
     * @param serverName
     * @param idItemMap
     */
    void syncOPCData(Server server, Connector connectionListener, String serverName, Map<String, Item> idItemMap);
}
