package com.xinda.xiaoxing.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinda.xiaoxing.dao.oracle.XxngPointMapper;
import com.xinda.xiaoxing.entity.oracle.XxngPoint;
import com.xinda.xiaoxing.service.IXxngPointService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author betacat
 * @since 2021-04-19
 */
@Service
public class XxngPointServiceImpl extends ServiceImpl<XxngPointMapper, XxngPoint> implements IXxngPointService {

}
