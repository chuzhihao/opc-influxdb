package com.xinda.xiaoxing.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xinda.xiaoxing.entity.oracle.XxngPoint;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author betacat
 * @since 2021-04-19
 */
public interface IXxngPointService extends IService<XxngPoint> {

}
