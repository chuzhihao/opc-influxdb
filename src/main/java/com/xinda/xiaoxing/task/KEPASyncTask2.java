//package com.xinda.xiaoxing.task;
//
//import com.xinda.xiaoxing.condition.KEPCondition2;
//import com.xinda.xiaoxing.config.opc.ConnectionListener;
//import com.xinda.xiaoxing.service.OPCService;
//import com.xinda.xiaoxing.util.ManualRegisterBeanUtil;
//import com.xinda.xiaoxing.util.MapUtil;
//import org.jinterop.dcom.common.JIException;
//import org.openscada.opc.lib.common.AlreadyConnectedException;
//import org.openscada.opc.lib.common.ConnectionInformation;
//import org.openscada.opc.lib.da.Item;
//import org.openscada.opc.lib.da.Server;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.context.annotation.Conditional;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.SchedulingConfigurer;
//import org.springframework.scheduling.config.CronTask;
//import org.springframework.scheduling.config.ScheduledTaskRegistrar;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//import java.net.UnknownHostException;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.Executors;
//
//@Conditional(KEPCondition2.class)
//@Component
//@EnableScheduling
//public class KEPASyncTask2 implements SchedulingConfigurer {
//
//    @Value("${opc.kep2.prefix}")
//    String kepPrefix2;
//    @Value("${opc.kep2.serverName}")
//    String kepServerName2;
//    @Value("${opc.kep2.threadNum}")
//    int threadNum;
//
//    @Autowired
//    ConnectionInformation kepConnectionInformation2;
//
//    @Autowired
//    Server kepServer2;
//
//    @Autowired
//    Map<String, Item> kepIdItemMap2;
//
//    List<Map<String,Item>> subKepIdItemMaps;
//
//    @Autowired
//    ConfigurableApplicationContext applicationContext;
//
//    ConnectionListener[] connectionListeners;
//    Server[] servers;
//
//    /**
//     * 初始化
//     */
//    @PostConstruct
//    public void init() {
//        try {
//            //获取总的IdItemMap
//            kepServer2.connect();
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        } catch (JIException e) {
//            e.printStackTrace();
//        } catch (AlreadyConnectedException e) {
//            e.printStackTrace();
//        }
//
//        servers=new Server[threadNum];
//        connectionListeners=new ConnectionListener[threadNum];
//        subKepIdItemMaps= MapUtil.mapChunkByDivisionNum(kepIdItemMap2,threadNum);
//
//        for(int i=0;i<threadNum;i++){
//            ManualRegisterBeanUtil.registerBean(applicationContext,kepServerName2+"-"+i,Server.class,
//                    kepConnectionInformation2,Executors.newSingleThreadScheduledExecutor());
//            Server server= (Server)applicationContext.getBean(kepServerName2+"-"+i);
//            servers[i]=server;
//
//            ManualRegisterBeanUtil.registerBean(applicationContext,"kepConnectionListener2-"+i,ConnectionListener.class,new Object[0]);
//            ConnectionListener connectionListener=(ConnectionListener)applicationContext.getBean("kepConnectionListener2-"+i);
//            connectionListener.setIdItemMap(kepIdItemMap2);
//            connectionListener.setPrefix(kepPrefix2);
//            connectionListener.setServerName(kepServerName2+"-"+i);
//            connectionListeners[i]=connectionListener;
//
//            server.addStateListener(connectionListener);
//
//            try {
//                servers[i].connect();
//            } catch (UnknownHostException | JIException | AlreadyConnectedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    @Override
//    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
//        for(int i=0;i<threadNum;i++){
//            OPCService opcService = (OPCService) applicationContext.getBean("OPCServiceImpl");
//            TaskThread taskThread=new TaskThread(opcService,servers[i],connectionListeners[i],kepServerName2+"-"+i,subKepIdItemMaps.get(i));
//            CronTask cronTask=new CronTask(taskThread,"*/5 * * * * ?");
//            scheduledTaskRegistrar.addCronTask(cronTask);
//        }
//    }
//}
