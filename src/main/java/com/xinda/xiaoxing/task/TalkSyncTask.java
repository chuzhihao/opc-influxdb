package com.xinda.xiaoxing.task;

import com.xinda.xiaoxing.condition.TalkCondition;
import com.xinda.xiaoxing.config.opc.ConnectionListener;
import com.xinda.xiaoxing.service.OPCService;
import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.UnknownHostException;
import java.util.Map;

@Conditional(TalkCondition.class)
@Component
@EnableScheduling
public class TalkSyncTask {

    @Autowired
    Server talkServer;
    @Autowired
    ConnectionListener talkConnectionListener;
    @Autowired
    Map<String, Item> talkIdItemMap;
    @Autowired
    OPCService talkOPCService;

    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        try {
            talkServer.connect();
        } catch (UnknownHostException | AlreadyConnectedException | JIException e) {
            e.printStackTrace();
        }
    }

    /**
     * 同步talk服务器数据
     */
    @Scheduled(cron = "*/10 * * * * ?")
    public void talkSync(){
        talkOPCService.syncOPCData(talkServer,talkConnectionListener,"talkServer",talkIdItemMap);
    }
}
