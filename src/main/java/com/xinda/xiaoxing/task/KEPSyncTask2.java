package com.xinda.xiaoxing.task;

import com.xinda.xiaoxing.condition.KEPCondition2;
import com.xinda.xiaoxing.config.opc.ConnectionListener;
import com.xinda.xiaoxing.service.OPCService;
import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.UnknownHostException;
import java.util.Map;

@Conditional(KEPCondition2.class)
@Component
@EnableScheduling
public class KEPSyncTask2 {

    @Autowired
    Server kepServer2;
    @Autowired
    ConnectionListener kepConnectionListener2;
    @Autowired
    Map<String, Item> kepIdItemMap2;
    @Autowired
    OPCService kepOPCService;

    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        try {
            kepServer2.connect();
        } catch (UnknownHostException | AlreadyConnectedException | JIException e) {
            e.printStackTrace();
        }
    }

    /**
     * 同步kep服务器数据
     */
    @Scheduled(cron = "*/5 * * * * ?")
    public void kepSync(){
        kepOPCService.syncOPCData(kepServer2,kepConnectionListener2,"kepServer2",kepIdItemMap2);
    }
}
