package com.xinda.xiaoxing.controller;

import com.xinda.xiaoxing.entity.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.common.NotConnectedException;
import org.openscada.opc.lib.da.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;
import java.util.Map;

@Slf4j
@RestController
public class PointController {
    @Autowired
    Server kepServer1;
    @Autowired
    Server kepServer2;
    @Autowired
    Server talkServer;

    @Autowired
    Map<String, Item> kepIdItemMap1;
    @Autowired
    Map<String, Item> kepIdItemMap2;
    @Autowired
    Map<String, Item> talkIdItemMap;

    @PostMapping("/point/add")
    public Result addPoint(String serverType,String channel,String device,String item){
        log.info("Add point:"+item);
        try {
            if(serverType.equals("kepServer1")){
                String itemId=channel+"."+device+"."+item;
                Group group = kepServer1.findGroup("default");
                Item itemObj = group.addItem(itemId);
                kepIdItemMap1.put(itemId,itemObj);
                log.info(kepIdItemMap1.toString());
            }else if(serverType.equals("kepServer2")){
                String itemId=channel+"."+device+"."+item;
                Group group = kepServer2.findGroup("default");
                Item itemObj = group.addItem(itemId);
                kepIdItemMap2.put(itemId,itemObj);
                log.info(kepIdItemMap2.toString());
            }else if(serverType.equals("talkServer")){
                String itemId="["+device+"]"+item;
                Group group = talkServer.findGroup("default");
                Item itemObj = group.addItem(itemId);
                talkIdItemMap.put(itemId,itemObj);
                log.info(talkIdItemMap.toString());
            }
            return Result.OK();
        } catch (JIException | AddFailedException | UnknownHostException | NotConnectedException e) {
            e.printStackTrace();
            return Result.error(e.getCause().toString());
        } catch (UnknownGroupException e) {
            e.printStackTrace();
        }
        return Result.error("添加失败");
    }

    @DeleteMapping("/point/delete")
    public Result delPoint(String serverType,String channel,String device,String item){
        log.info("Delete point:"+item);
        try {
            if(serverType.equals("kepServer1")){
                String itemId=channel+"."+device+"."+item;
                Group group = kepServer1.findGroup("default");
                group.removeItem(itemId);
                kepIdItemMap1.remove(itemId);
                log.info(kepIdItemMap1.toString());
            }else if(serverType.equals("kepServer2")){
                String itemId=channel+"."+device+"."+item;
                Group group = kepServer2.findGroup("default");
                group.removeItem(itemId);
                kepIdItemMap2.remove(itemId);
                log.info(kepIdItemMap2.toString());
            }else if(serverType.equals("talkServer")){
                String itemId="["+device+"]"+item;
                Group group = talkServer.findGroup("default");
                group.removeItem(itemId);
                talkIdItemMap.remove(itemId);
                log.info(talkIdItemMap.toString());
            }
            return Result.OK();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (JIException e) {
            e.printStackTrace();
        } catch (UnknownGroupException e) {
            e.printStackTrace();
        } catch (NotConnectedException e) {
            e.printStackTrace();
        }
        return Result.error("删除失败");
    }

}
