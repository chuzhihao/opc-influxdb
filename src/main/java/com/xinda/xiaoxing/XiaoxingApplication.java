package com.xinda.xiaoxing;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xinda.xiaoxing.dao")
public class XiaoxingApplication {

    public static void main(String[] args) {
        SpringApplication.run(XiaoxingApplication.class, args);
    }

}
