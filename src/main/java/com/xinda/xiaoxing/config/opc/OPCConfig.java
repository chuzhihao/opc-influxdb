package com.xinda.xiaoxing.config.opc;

import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

@Configuration
public class OPCConfig {

    //==================================KEP1配置==================================
    @Value("${opc.kep1.prefix}")
    String kepPrefix1;
    @Value("${opc.kep1.serverName}")
    String kepServerName1;

    @Bean
    @ConfigurationProperties("opc.kep1")
    public ConnectionInformation kepConnectionInformation1(){
        return new ConnectionInformation();
    }

    @Bean
    public Map<String, Item> kepIdItemMap1(){
        return new HashMap<>();
    }

    @Bean
    public ConnectionListener kepConnectionListener1(Map<String, Item> kepIdItemMap1){
        ConnectionListener kepConnectionListener=new ConnectionListener();
        kepConnectionListener.setIdItemMap(kepIdItemMap1);
        kepConnectionListener.setPrefix(kepPrefix1);
        kepConnectionListener.setServerName(kepServerName1);
        return kepConnectionListener;
    }

    @Bean
    public Server kepServer1(ConnectionInformation kepConnectionInformation1, ConnectionListener kepConnectionListener1){
        Server server= new Server(kepConnectionInformation1, Executors.newSingleThreadScheduledExecutor()); // 启动服务
        server.addStateListener(kepConnectionListener1);
        return server;
    }

    //==================================KEP1配置结束==================================

    //==================================KEP2配置==================================
    @Value("${opc.kep2.prefix}")
    String kepPrefix2;
    @Value("${opc.kep2.serverName}")
    String kepServerName2;

    @Bean
    @ConfigurationProperties("opc.kep2")
    public ConnectionInformation kepConnectionInformation2(){
        return new ConnectionInformation();
    }

    @Bean
    public Map<String, Item> kepIdItemMap2(){
        return new HashMap<>();
    }

    @Bean
    public ConnectionListener kepConnectionListener2(Map<String, Item> kepIdItemMap2){
        ConnectionListener kepConnectionListener=new ConnectionListener();
        kepConnectionListener.setIdItemMap(kepIdItemMap2);
        kepConnectionListener.setPrefix(kepPrefix2);
        kepConnectionListener.setServerName(kepServerName2);
        return kepConnectionListener;
    }

    @Bean
    public Server kepServer2(ConnectionInformation kepConnectionInformation2, ConnectionListener kepConnectionListener2){
        Server server= new Server(kepConnectionInformation2, Executors.newSingleThreadScheduledExecutor()); // 启动服务
        server.addStateListener(kepConnectionListener2);
        return server;
    }

    //==================================KEP2配置结束==================================


    //==================================TALK配置==================================
    @Value("${opc.talk.prefix}")
    String talkPrefix;
    @Value("${opc.talk.serverName}")
    String talkServerName;

    @Bean
    @ConfigurationProperties("opc.talk")
    public ConnectionInformation talkConnectionInformation(){
        return new ConnectionInformation();
    }

    @Bean
    public Map<String, Item> talkIdItemMap(){
        return new HashMap<>();
    }

    @Bean
    public ConnectionListener talkConnectionListener(Map<String, Item> talkIdItemMap){
        ConnectionListener talkConnectionListener=new ConnectionListener();
        talkConnectionListener.setIdItemMap(talkIdItemMap);
        talkConnectionListener.setPrefix(talkPrefix);
        talkConnectionListener.setServerName(talkServerName);
        return talkConnectionListener;
    }

    @Bean
    public Server talkServer(ConnectionInformation talkConnectionInformation, ConnectionListener talkConnectionListener){
        Server server= new Server(talkConnectionInformation, Executors.newSingleThreadScheduledExecutor()); // 启动服务
        server.addStateListener(talkConnectionListener);
        return server;
    }

    //==================================TALK配置结束==================================

}
