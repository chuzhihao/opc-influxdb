package com.xinda.xiaoxing.config.opc;

public interface Connector {
    boolean isConnected();
}
