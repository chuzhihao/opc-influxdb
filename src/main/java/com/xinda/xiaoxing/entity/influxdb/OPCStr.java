//package com.xinda.xiaoxing.entity.influxdb;
//
//import lombok.Data;
//import org.influxdb.annotation.Column;
//import org.influxdb.annotation.Measurement;
//
//@Data
//@Measurement(name = "opc_str")
//public class OPCStr {
//
//    @Column(name="channel",tag=true)
//    private String channel;
//
//    @Column(name="device",tag = true)
//    private String device;
//
//    @Column(name="item",tag = true)
//    private String item;
//
//    @Column(name = "server",tag = true)
//    private String server;
//
//    @Column(name = "value")
//    private String value;
//
//    @Column(name="time")
//    private String time;
//}
